import React, { Component } from 'react';

import { Text, View, Image } from 'react-native';



const theme = {

    Button: {

      titleStyle: {

        backgroundColor: '#2e8b57',

      },

    },

  };



var styles = {

    wrapper: {

    },

    bigTitle: {

        fontWeight: 'bold',

        fontSize: 32,

    },

    subTitle: {

        fontWeight: 'bold',

        fontSize: 20,

        textAlign: 'center'

    },

    skip: {

        marginTop: 30,

        fontSize: 18,

        color: '#b1b1b1',

    },

    buttom: {

        backgroundColor: "#2e8b57",

    },

    backGroundColor: {

        flex: 1,

        justifyContent: "center",

        alignItems: "center",

        backgroundColor: "linear-gradient(to right top, #d16ba5, #c777b9, #ba83ca, #aa8fd8, #9a9ae1, #8aa7ec, #79b3f4, #69bff8, #52cffe, #41dfff, #46eefa, #5ffbf1)"

    },

    bottomTitle: {

        marginTop: 200,

        fontSize: 18,

    }

}



export default class ReportComplete extends Component {

    constructor(props) {

        super(props);

        this.state = {

            text: ''

        };

    }

    render() {

        return (

            <View style={styles.backGroundColor}>

                <Image

                    style={{ width: 150, height: 150 }}

                    source={require('../assets/world.png')}

                />

                <Text style={styles.subTitle}>You are one of our heroes</Text>

                <Text style={styles.bigTitle}>WHO ARE YOU?</Text>

                <Text style={styles.bottomTitle}

                onPress={() =>

                    this.props.navigation.navigate('loadApp')

                }

                >Opening camera ...</Text>

            </View>

        );

    }

}
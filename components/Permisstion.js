import React, { Component } from 'react';
import { Text, View, TextInput, Image, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
var styles = {
    wrapper: {
    },
    bigTitle: {
        fontWeight: 'bold',
        fontSize: 32,
    },
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    skip: {
        marginTop: 30,
        fontSize: 18,
        color: '#b1b1b1',
    },
    button: {
        width: 300,
        height: 44,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#1E6738',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
    },
}

export default class PermissionCamera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        };
    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Image
                    style={{ width: 250, height: 250 }}
                    source={require('../assets/step4.png')}
                />
                <Text style={styles.bigTitle}>Enable Your Location</Text>
                <Text style={styles.subTitle}>Choose your location to start find the
find the garbage around you
                </Text>
                <Button
                    title={'USE MY LOCATION'}
                    onPress={ () => this.useLocation() }
                    buttonStyle={styles.button}
                />
                <Text style={styles.skip}
                    onPress={() =>
                        this.props.navigation.navigate('homeQrLoad')
                    }
                >Skip for now</Text>
            </View>
        );
    }

    async useLocation() {

            // set permission location
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            alert('Permission to access location was denied');
        }

        Location.getCurrentPositionAsync({}).then(location => {
            const myLocation = {
                lat: location.coords.latitude,
                lng: location.coords.longitude
            };
            fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + myLocation.lat + ',' + myLocation.lng + '&key=' + 'AIzaSyDEQcrVnA9xZbCG1MgYH7uIR2OpT_LaG_Y')
            .then(res =>res.json()).then(async responseJSON => {
                await AsyncStorage.setItem('myAddress', responseJSON.results[0].formatted_address);
                await AsyncStorage.setItem('myLocation', JSON.stringify(myLocation));

            })
        });
        this.props.navigation.navigate('login');
    }
}
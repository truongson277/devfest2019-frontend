import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
  Platform,
  TouchableOpacity,
  AsyncStorage,
  Modal,
  TouchableHighlight,
  Alert
} from 'react-native';
import MapView from 'react-native-maps';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { Makers } from '../data/makers';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';

export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      makers: Makers,
      user: {
        lat: 0,
        lng: 0,
        avatar: { uri: 'https://appdata.chatwork.com/avatar/2215/2215848.rsz.jpg' }
      },
      hasCameraPermission: '',
      hasLocationPermission: '',
      type: Camera.Constants.Type.back,
      modalVisible: false,
    }
  }
  static navigationOptions = {
    title: 'Map',
    navigationOptions: {
      headerVisible: false
    }
  };

  async componentWillMount() {
    console.log('MAP');
    const location = JSON.parse(await AsyncStorage.getItem('myLocation'));
    const user = JSON.parse(await AsyncStorage.getItem('userInfo'));
    const newMakers = this.state.makers.concat([{
      id: 10,
      lat: location.lat,
      lng: location.lng,
      status: 'avatar',
      addr: ''
    }]);
    this.setState({
      makers: newMakers
    });
    this.setState({
      tab: 1
    });
    const myLocation = JSON.parse(await AsyncStorage.getItem('myLocation'));
    this.setState({
      user: {
        lat: myLocation.lat,
        lng: myLocation.lng,
        avatar: { uri: user.picture.data.url }
      }
    });

    console.log(this.state.user.avatar);
  }
  render() {
    return (
      <View>
      <ScrollView>
        <SafeAreaView style={[styles.tabs]}>
          <Text

            onPress={() => {
              this.onChangeTab(1);
            }}
            style={[styles.tab, this.state.tab === 1 ? styles.tabActive : null]}>
            Map
          </Text>
          <Text
            onPress={() => {
              this.onChangeTab(2);
            }}
            style={[styles.tab, this.state.tab === 2 ? styles.tabActive : null]}>
            Top List{' '}
          </Text>
        </SafeAreaView>
        {this.state.tab === 2 ? (
          <View style={styles.container}>
            {this.state.makers.map((item, index) => (
              <View style={styles.item} key={index}>
                <View style={styles.itemInfo}>
                  <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                    {item.addr}
                  </Text>
                  <Text style={{ fontSize: 13, color: '#2e8b57' }}>
                    Nearest
                  </Text>
                  <View style={{ marginTop: 15, flexDirection: 'row' }}>
                    <Text
                      style={{
                        fontSize: 13,
                        color: '#f34242',
                        textTransform: 'uppercase',
                        fontWeight: 'bold',
                        width: 130
                      }}>
                      Overflow
                    </Text>
                    <Text
                      style={{
                        color: '#707070',
                        textTransform: 'lowercase',
                      }}>
                      {/* <Ionicons name="circle" size={13} color="#707070" /> */}
                      <Text style={{ marginLeft: 5 }}> 0.2 km</Text>
                    </Text>
                  </View>
                </View>
                <View style={styles.itemImage}>
                  <Image
                    style={{ width: 50, height: 50 }}
                    source={require('../assets/qr-code.png')}
                  />
                </View>
              </View>
            ))}
          </View>
        ) : (
            <View>
              <MapView
                style={{ flex: 1, width: '100%', height: 1000 }}
                region={{
                  latitude: this.state.user.lat,
                  longitude: this.state.user.lng,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
              >
                {this.state.makers.map((maker, index) => {
                  const coordinate = {
                    latitude: maker.lat,
                    longitude: maker.lng
                  };
                  let mapMaker;
                  if (maker.status === 'ok') {
                    mapMaker = (<MapView.Marker key={index}
                      identifier="restaurantPosition"
                      coordinate={coordinate}
                      image={require('../assets/ok/ok.png')}
                    />
                    );
                  } else if (maker.status === 'warning') {
                    mapMaker = (<MapView.Marker key={index}
                      identifier="restaurantPosition"
                      coordinate={coordinate}
                      image={require('../assets/warning/warning.png')}
                    />
                    );
                  } else if (maker.status === 'danger') {
                    mapMaker = (<MapView.Marker key={index}
                      identifier="restaurantPosition"
                      coordinate={coordinate}
                      image={require('../assets/danger/danger.png')}
                    />
                    );
                  } else {
                    mapMaker = (<MapView.Marker key={index}
                      identifier="restaurantPosition"
                      coordinate={coordinate}

                    >
                      <Image source={this.state.user.avatar} style={{ height: 35, width: 35 }}></Image>
                    </MapView.Marker>
                    );
                  }

                  return mapMaker;
                })
                }
              </MapView>
            </View>
          )}
      </ScrollView>
      <Modal transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal}>
        <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'}}>
          <View style={{
                  backgroundColor: '#fff',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 300,
                  borderRadius: 14,
                  height: 300}}>
            <Text style={{ fontSize: 22, color: '#99abb4' }}>Choose your option</Text>
            <Text style={{ fontSize: 14, color: '#99abb4' }}>Report with us using camera on your phone</Text>
            <View style={{ flexDirection: 'row', marginTop: 30 }}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('ScannerQRcode'); this.setState({ modalVisible: false }) }}>
                <Image style={{ width: 64, height: 64, marginRight: 60 }} source={require('../assets/qr-code-1.png')} />
                <Text style={{ fontWeight: 'bold', fontSize: 16, marginTop: 10 }}>QR CODE</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('Camera'); this.setState({ modalVisible: false }) }}>
                <Image style={{ width: 64, height: 64 }} source={require('../assets/camera-logo.png')} />
                <Text style={{ fontWeight: 'bold', fontSize: 16, marginTop: 10 }}>CAMERA</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <TouchableOpacity onPress={() => {
        this.selectCamera();
      }} style={{ position: 'absolute', top: '80%', left: '40%', borderRadius: 32, zIndex: 9999999999999, justifyContent: 'center', alignItems: 'center', width: 64, height: 64, backgroundColor: '#e3624c' }}>
        <Image style={{ width: 24, height: 24 }} source={require('../assets/button-camera.png')}></Image>
      </TouchableOpacity>
      </View>
    );
  }

  onChangeTab(tab) {
    this.setState({ tab: tab });
  }

  async selectCamera() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ modalVisible: true })
  }
}

const styles = StyleSheet.create({
  tabs: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: Platform.OS === 'android' ? 25 : 0
  },
  tab: {
    width: '50%',
    textAlign: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 18,
    fontWeight: 'bold',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    color: '#707070',
  },
  tabActive: {
    borderBottomColor: '#2e8b57',
    borderBottomWidth: 2,
    color: '#2e8b57',
  },
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 15,
    flex: 1,
    marginBottom: 30,
  },
  item: {
    flexDirection: 'row',
    borderColor: '#2e8b57',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    padding: 15,
    marginBottom: 10,
  },
  itemInfo: {
    width: '80%',
  },
  itemImage: {
    width: '20%',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});

import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView, AsyncStorage, ActivityIndicator,TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { send } from '../api/send';
export default class Report extends Component {
    static navigationOptions = {
        title: 'Reports',
    }
    constructor(props) {
        super(props);
        this.state = {
            status: 'clean',
            note: '',
            checked: false,
            data: [
                {
                    label: 'Clean',
                },
                {
                    label: 'Warning',
                },
                {
                    label: 'Overflow',
                },
            ],
            photo: {},
            myLocation: '',
            userInfo: { },
            picture: '',
            isReady: false,
            isSave: false
        };
    }

    async componentWillMount() {
        const myLocation = JSON.parse(await AsyncStorage.getItem('myLocation'));
        const myAddress = await AsyncStorage.getItem('myAddress');
        const userInfo = JSON.parse(await AsyncStorage.getItem('userInfo'));
        const photo = this.props.navigation.getParam('photo');
        const qr_code_info = this.props.navigation.getParam('qr_code_info');
        
        this.setState({ myLocation: myLocation});
        this.setState({ myAddress: myAddress });
        this.setState({ photo: photo });
        this.setState({ qr_code_info: qr_code_info });
        this.setState({ userInfo: userInfo });
        this.setState({
            uri: {
                uri: this.state.userInfo.picture.data.url
            }
        });     
        if (this.state.qr_code_info) {
            this.setState({
                myAddress: this.state.qr_code_info.attributes.address,
                myLocation: {
                    lat: this.state.qr_code_info.attributes.latitude,
                    lng: this.state.qr_code_info.attributes.longitude
                }
            })
        } 
    }
    onPress = status => this.setState({ status });
    onSubmit() {
        console.log('start report ...');
        this.report();
    }

    getStatus(){
        const status = this.state.data.filter(d => d.selected).map(d => d.label)[0];
        this.setState({ status: status.toLocaleLowerCase()})
    }

    report() {
        // call API
        this.setState({ isSave: true })
        this.setState({ isReady: true });
        const body = new FormData();
        body.append('report[status]', this.state.status);
        body.append('report[note]', this.state.note);
        body.append('report[report_type]','report');
        body.append('report[address]', this.state.myAddress);
        body.append('report[latitude]', this.state.myLocation.lat);
        body.append('report[longitude]', this.state.myLocation.lng);
        
       
        body.append('report[image]', {
            uri: this.state.photo.uri,
            type: 'image/jpg',
            name: 'test.jpg'
        });
        // body.append('report[garbage_id]','123');
        // body.append('report[landfill_id]','123');
        send('POST', {
            url: '/api/v1/reports', body: body
        }).then(res => {
            console.log('res', res);
            if (res.status === 200) {
                this.setState({ isReady: false });
                Alert.alert(
                    'REPORT',
                    'Thanks for your report!',
                    [
                        {text: 'OK', onPress: () => {  this.props.navigation.navigate('History'); }},
                    ]
                    );
               
            }
        }).catch(err => {
            this.setState({ isReady: false, isSave: false });
            alert(err);
        })
    }
    render() {
        let selectedButton = this.state.data.find(e => e.selected == true);
        selectedButton = selectedButton ? selectedButton.value : this.state.data[0].label;
        return (
            <ScrollView>
                <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "flex-start", marginTop: 20 }}>
                    <Text style={styles.bigTitle}>
                        Checkin
                    </Text>
                    <Text style={{ marginLeft: 15, color: '#99abb4' }}
                    >
                        { this.state.myAddress }
                </Text>
                    <Text style={styles.bigTitle}>
                        My heroes
                </Text>
                    <View style={{ flexDirection: 'row', marginLeft: 15, marginTop: 10, marginBottom: 10, alignItems: "center" }}>
                        <Image
                            style={{ width: 50, height: 50,borderRadius: 10 }}
                            source={this.state.uri}
                        />
                        <Text style={{ marginLeft: 15, color: '#99abb4' }}
                        >
                           { this.state.userInfo.name }
                </Text>
                    </View>
                    <Text style={styles.bigTitle}>
                        Status
                    </Text>
                    <View style={styles.container}>
                        <RadioGroup onPress={ () => this.getStatus() } radioButtons={this.state.data} value={this.state.status} flexDirection='row' justifyContent='center' />
                    </View>
                    <Text style={styles.bigTitle}>
                        Your photo
                    </Text>
                    <Image resizeMode={'contain'}
                        style={{
                            width: '100%',
                            height: 220

                        }}
                        source={{ uri: this.state.photo.uri }}
                    />
                    <Text style={styles.bigTitle}>
                        Note
                    </Text>
                    <TextInput
                        style={{ width: '90%', paddingBottom: 80, borderColor: 'gray', borderWidth: 1, marginLeft: 15, borderRadius: 10, paddingLeft: 15 }}
                        placeholder="Let's we know how your feeling ..."
                        multiline
                        value={this.state.note}
                        onChangeText={(note) => this.setState({ note })}
                    // numberOfLines={4}
                    />
                    <TouchableOpacity
                        disabled={this.state.isSave}
                        title={'Save'}
                        style={styles.button}
                        onPress={this.onSubmit.bind(this)}
                    >
                     <Text style={styles.textSave}>Save</Text>   
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        title={'Cancel'}
                        style={styles.cancel}
                        onPress={this.onSubmit.bind(this)}
                    >
                         <Text style={styles.textCancel}>Cancel</Text>
                    </TouchableOpacity>

                    {this.state.isReady ? (<View style={[styles.loader.container, styles.loader.horizontal]}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>) : null}
                </View>
                
            </ScrollView>
        );
    }
}

var styles = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    bigTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        textAlign: 'left',
        marginLeft: 15,
        marginTop: 15, 
        marginBottom: 15
    },
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'flex-start',
        marginLeft: 15,
        marginTop: 15, 
        marginBottom: 15
    },
    skip: {
        marginTop: 30,
        fontSize: 18,
        color: '#b1b1b1',
    },
    button: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#1E6738',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        marginLeft: 30,
       
    },
    textSave: {
        textAlign: 'center',
        color: '#fff'
    },
    textCancel: {
        textAlign: 'center'
    },
    cancel: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#f3f4f5',
        backgroundColor: '#f3f4f5',
        marginLeft: 30,
        marginBottom: 30,
        textAlign: 'center',
    },
    loader: {
        container: {
            position: 'absolute',
            top: '50%',
            left: '35%',
            flex: 1,
            justifyContent: 'center'
          },
          horizontal: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            padding: 10
          }        
    }
}
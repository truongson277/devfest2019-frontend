import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView } from 'react-native';
import { Constants } from 'expo';
import { Button } from 'react-native-elements';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { Users } from '../data/users';

var styles = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputSearch: {
        width: 300,
        height: 46,
        borderColor: '#99abb4',
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        textAlign: 'center',
        marginTop: 10,
    },
    flexRow: {
        flexDirection: 'row'
    },
    iconStyle: {
        width: 40,
        height: 40,
        marginTop: 5,
        marginRight: 10
    },
    textStyle: {
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10
    },
    imageHeader: {
        width: 300,
        height: 180,
        marginTop: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    titleHeader: {
        fontSize: 20,
        color: '#fff',
    },
    subtitleHeader: {
        fontSize: 13,
        marginTop: 5,
        color: '#fff',
    },
    signIn: {
        backgroundColor: '#2e8b57',
        borderRadius: 30,
        textAlign: 'center',
        paddingTop: 20,
        paddingBottom: 20,
        marginTop: 40,
    },
    signInText: {
        fontSize: 16,
        borderRadius: 30,
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    cancel: {
        backgroundColor: '#fff',
        borderRadius: 30,
        textAlign: 'center',
        paddingTop: 20,
        paddingBottom: 20,
    },
    cancelText: {
        fontSize: 16,
        borderRadius: 30,
        color: '#5f6d77',
        textAlign: 'center',
        fontWeight: 'bold',
    }

}
var tempCheckValues = [];
export default class CreateEventInvite extends Component {
    static navigationOptions = {
        title: 'Invite Friends',
    }
    constructor(props) {
        super(props);
        this.state = {
            users: Users,
            value: '',
            search: '',
            text: '',
            checkBoxChecked: [],
            userData: []
        };
    }

    componentWillMount() {
        this.setState({ users: Users });
        // console.log(this.state.users);
    }

    checkBoxChanged(id, value) {
        console.log(id, value);

        this.state.users[id-1].invite = !value;
        var userData2 = this.state.users;
        this.setState({ users: userData2 })
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <View style={{ flexDirection: 'row', paddingLeft: 32, paddingRight: 32, paddingTop: 16, paddingBottom: 16 }}>
                        <CheckBox
                            value={this.state.checked}
                            onValueChange={() => this.setState({ checked: !this.state.checked })}
                        />
                        <View style={{ flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
                            <Text>
                                Mọi người xung quanh vị trí của bạn
                            </Text>
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: "center", }}>
                        {/* <Image
                            style={ styles.iconStyle }
                            source={require('../assets/icons/placeholder.png')}
                        /> */}
                        <TextInput
                            style={styles.inputSearch}
                            onChangeText={
                                text => onChangeText(this.state.value = text)
                            }
                            placeholder='Please input name friends'
                            value={this.state.value}
                        />
                    </View>
                    {
                        this.state.users.map((item, index) => {
                            return (<View style={{ flexDirection: 'row', paddingLeft: 32, paddingRight: 32, paddingTop: 16, paddingBottom: 16 }} key={index}>
                                <Image
                                    style={styles.iconStyle}
                                    source={ item.avatar }
                                />
                                <View style={{ flexDirection: 'column',flex: 1, justifyContent: 'flex-end' }}>
                                    <Text>
                                        {item.username}
                                    </Text>
                                </View>
                                <View style={{ flexDirection: 'column', flex: 1, alignItems: 'flex-end' , justifyContent: 'flex-end' }}>
                                    <CheckBox
                                        style={{ marginTop: 10 }}
                                        value={item.invite}
                                        onValueChange={() => this.checkBoxChanged(item.id, item.invite)}
                                        // onValueChange={() => this.setState({ checked: !this.state.checked })}
                                    />
                                </View>
                                
                            </View>)
                        }
                        )
                    }
                    <View style={styles.signIn}>
                        <Text style={styles.signInText} onPress={() => { 
                            this.state.users.map((item) => {
                                if (item.invite === true) {
                                    this.state.userData.push(item);
                                }
                            });
                            // console.log('@@@@@', this.state.userData); 
                            this.props.navigation.navigate('CreateEvent', {
                                users: this.state.userData
                            });
                        }}>Invite</Text>
                    </View>
                    <View style={styles.cancel}>
                        <Text style={styles.cancelText} onPress={() => { console.log('@@@@@'); }}>Cancel</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
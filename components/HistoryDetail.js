import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView, AsyncStorage, ActivityIndicator } from 'react-native';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { send } from '../api/send';
export default class Report extends Component {
    static navigationOptions = {
        title: 'Today',
    }
    constructor(props) {
        super(props);
        this.state = {
            status: 'clean',
            note: '',
            checked: false,
            data: [
                {
                    label: 'Clean',
                },
                {
                    label: 'Warning',
                },
                {
                    label: 'Overflow',
                },
            ],
            myLocation: '',
            userInfo: { },
            isReady: false
        };
    }

    async componentWillMount() {
        const report_id = this.props.navigation.getParam('report_id');
        this.setState({
            userInfo: JSON.parse( await AsyncStorage.getItem('userInfo'))
        });
        
        send('GET', { url: '/api/v1/report/' + report_id }).then(res => {
            if (res.status === 200) {
                this.setState({ report_info: res.data });
                console.log(this.state.report_info);
                this.setState({ uri: {uri: res.data.attributes.image_detect }});
                this.setState({ avatar: {uri: this.state.userInfo.picture.data.url}});
            }
        })
    }

    render() {
        
        return (
            this.state.report_info ? (
            <ScrollView style={{ paddingBottom: 60 }}>
                <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "flex-start", marginTop: 20 }}>
                    <Text style={styles.bigTitle}>
                        Checkin
                    </Text>
                    <Text style={{ marginLeft: 15, color: '#99abb4' }}
                    >
                        {this.state.report_info.attributes.address}
                </Text>
                    <Text style={styles.bigTitle}>
                        My heroes
                </Text>
                    <View style={{ flexDirection: 'row', marginLeft: 15, marginTop: 10, marginBottom: 10, alignItems: "center" }}>
                        <Image
                            style={{ width: 50, height: 50,borderRadius: 10 }}
                            source={this.state.avatar}
                        />
                        <Text style={{ marginLeft: 15, color: '#99abb4' }}
                        >
                          {this.state.userInfo.name}
                </Text>
                    </View>
                    <Text style={styles.bigTitle}>
                        Status
                    </Text>
                    <Text style={{ marginLeft: 15, color: 'green' }}>{this.state.report_info.attributes.status}</Text>
                    <Text style={styles.bigTitle}>
                        Your photo
                    </Text>
                    <Image resizeMode={'contain'}
                        style={{
                            width: '100%',
                            height: 220

                        }}
                        source={this.state.uri}
                    />
                    <Text style={styles.bigTitle}>
                        Note
                    </Text>
                    <Text style={{ paddingLeft: 15, paddingRight: 15 }}>{this.state.report_info.attributes.note}</Text>
                </View>
                
            </ScrollView>
            ) : null
        );
        
    }
}

var styles = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    bigTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        textAlign: 'left',
        marginLeft: 15,
        marginTop: 15, 
        marginBottom: 15
    },
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'flex-start',
        marginLeft: 15,
        marginTop: 15, 
        marginBottom: 15
    },
    skip: {
        marginTop: 30,
        fontSize: 18,
        color: '#b1b1b1',
    },
    button: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#1E6738',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        marginLeft: 30
    },
    cancel: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#f3f4f5',
        backgroundColor: '#f3f4f5',
        marginLeft: 30,
        marginBottom: 30
    },
    loader: {
        container: {
            position: 'absolute',
            top: '50%',
            left: '35%',
            flex: 1,
            justifyContent: 'center'
          },
          horizontal: {
            flexDirection: 'row',
            justifyContent: 'space-around',
            padding: 10
          }        
    }
}
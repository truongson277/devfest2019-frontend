import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  SafeAreaView,
  Platform,
  TouchableOpacity,
  AsyncStorage,
  Modal,
  TouchableHighlight,
  Alert
} from 'react-native';
import MapView from 'react-native-maps';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { Events } from '../data/events';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';

export default class EventList2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: Events,
      user: {
        lat: 0,
        lng: 0,
        avatar: { uri: 'https://appdata.chatwork.com/avatar/2215/2215848.rsz.jpg' }
      },
      hasCameraPermission: '',
      hasLocationPermission: '',
      type: Camera.Constants.Type.back,
      modalVisible: false,
    }
  }
  static navigationOptions = {
    title: 'Event List',
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  async componentWillMount() {
    const location = JSON.parse(await AsyncStorage.getItem('myLocation'));
    const user = JSON.parse(await AsyncStorage.getItem('userInfo'));
    // const newMakers = this.state.makers.concat([{
    //   id: 10,
    //   lat: location.lat,
    //   lng: location.lng,
    //   status: 'avatar',
    //   addr: ''
    // }]);
    this.setState({
      // makers: newMakers
    });
    this.setState({
      tab: 1
    });
    const myLocation = JSON.parse(await AsyncStorage.getItem('myLocation'));
    this.setState({
      user: {
        lat: myLocation.lat,
        lng: myLocation.lng,
        avatar: { uri: user.picture.data.url }
      }
    });

    console.log(this.state.user.avatar);
  }
  render() {
    return (
      <View>
      <ScrollView>
        <SafeAreaView style={[styles.tabs]}>
          <Text

            onPress={() => {
              this.onChangeTab(1);
            }}
            style={[styles.tab, this.state.tab === 1 ? styles.tabActive : null]}>
            This Week
          </Text>
          <Text
            onPress={() => {
              this.onChangeTab(2);
            }}
            style={[styles.tab, this.state.tab === 2 ? styles.tabActive : null]}>
            Up Comming{' '}
          </Text>
        </SafeAreaView>
        {this.state.tab === 2 ? (
          <View style={styles.container}>
            <View style={{ marginTop: 5, marginBottom: 5}}>
                <Image
                style={ styles.imageHeader }
                source={{uri: 'https://images.unsplash.com/photo-1570654282300-9e8952986614?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80'}}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: -40 }}>
                <Text style={ styles.titleHeader }>
                Fireworks Night festival
                </Text>
                <Text style={ styles.subtitleHeader }>
                2 miles
                </Text>
                </View>
                
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/calendar.png')}
                        />
                        <Text style={ styles.textStyle }>
                        Mon, 10 Oct 2019
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/time.png')}
                        />
                        <Text style={ styles.textStyle }>
                        19:00
                        </Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderBottomColor: 'rgba(0,0,0,0.16)', borderBottomWidth: 0.5, paddingBottom: 10 }}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/placeholder.png')}
                        />
                        <Text style={ styles.textStyle }>
                        {/* {item.addr} */}
                        </Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={{ width: 30, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                        source={require('../assets/icons/threePeople.png')}
                        />
                        <Text style={ styles.textStyle }>
                        42 Interested
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={{ width: 20, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                        source={require('../assets/icons/eye.png')}
                        />
                        <Text style={ styles.textStyle }>
                        210 views
                        </Text>
                    </View>
                </View>
            </View>
          {this.state.events.map((item, index) => (
            <View style={styles.item} key={index}>
              <View style={styles.itemImage}>
                <Image
                  style={{ width: 50, height: 50 }}
                  source={{ uri: 'https://images.unsplash.com/photo-1570864856537-72409c00759f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80'}}
                />
              </View>
              <View style={styles.itemInfo}>
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                  {item.addr}
                </Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff',fontSize: 13, color: '#2e8b57' }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/calendar.png')}
                        />
                        <Text style={ styles.textStyle }>
                        {item.date}
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/time.png')}
                        />
                        <Text style={ styles.textStyle }>
                        {item.time}
                        </Text>
                    </View>
                </View>
                <View style={{ marginTop: 15, flexDirection: 'row' }}>
                  <Text
                    style={{
                      color: '#707070',
                      textTransform: 'lowercase',
                    }}>
                      <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/placeholder.png')}
                      />
                    <Text style={{ marginLeft: 5 }}> {item.addr}</Text>
                  </Text>
                </View>
              </View>
            </View>
          ))}
        </View>
          ) : (
          <View style={styles.container}>
            <Text style={{ fontSize: 24, color: '#000'}}>
            Top Events
            </Text>
            <View style={{ marginTop: 5, marginBottom: 5}}>
                <Image
                style={ styles.imageHeader }
                source={{uri: 'https://images.unsplash.com/photo-1570654282300-9e8952986614?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80'}}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: -40 }}>
                <Text style={ styles.titleHeader }>
                Fireworks Night festival
                </Text>
                <Text style={ styles.subtitleHeader }>
                2 miles
                </Text>
                </View>
                
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/calendar.png')}
                        />
                        <Text style={ styles.textStyle }>
                        Mon, 10 Oct 2019
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/time.png')}
                        />
                        <Text style={ styles.textStyle }>
                        19:00
                        </Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderBottomColor: 'rgba(0,0,0,0.16)', borderBottomWidth: 0.5, paddingBottom: 10 }}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/placeholder.png')}
                        />
                        <Text style={ styles.textStyle }>
                        Fireworks Night festival
                        </Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={{ width: 30, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                        source={require('../assets/icons/threePeople.png')}
                        />
                        <Text style={ styles.textStyle }>
                        42 Interested
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={{ width: 20, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                        source={require('../assets/icons/eye.png')}
                        />
                        <Text style={ styles.textStyle }>
                        210 views
                        </Text>
                    </View>
                </View>
            </View>
            <Text style={{ fontSize: 24, color: '#000', marginBottom: 5}}>
            Popular events nearby
            </Text>
          {this.state.events.map((item, index) => (
            <View style={styles.item} key={index}>
              <View style={styles.itemImage}>
                <Image
                  style={{ width: 70, height: 70 }}
                  source={{ uri: 'https://images.unsplash.com/photo-1570864856537-72409c00759f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80'}}
                />
              </View>
              <View style={styles.itemInfo}>
                <Text style={{ fontSize: 15, fontWeight: 'bold' }}>
                  {item.addr}
                </Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff',fontSize: 13, color: '#2e8b57' }}>
                    <View style={ styles.flexRow}>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/calendar.png')}
                        />
                        <Text style={ styles.textStyle }>
                        {item.date}
                        </Text>
                    </View>
                    <View style={ styles.flexRow }>
                        <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/time.png')}
                        />
                        <Text style={ styles.textStyle }>
                        {item.time}
                        </Text>
                    </View>
                </View>
                <View style={{ marginTop: 15, flexDirection: 'row' }}>
                  <Text
                    style={{
                      color: '#707070',
                      textTransform: 'lowercase',
                    }}>
                      <Image
                        style={ styles.iconStyle }
                        source={require('../assets/icons/placeholder.png')}
                      />
                    <Text style={{ marginLeft: 5 }}> {item.addr}</Text>
                  </Text>
                </View>
              </View>
            </View>
          ))}
        </View>
          )}
      </ScrollView>
      <TouchableOpacity onPress={() => {
        console.log('@@@@@');
        this.props.navigation.navigate('CreateEvent')
      }} style={{ position: 'absolute', top: '80%', left: '40%', borderRadius: 32, zIndex: 9999999999999, justifyContent: 'center', alignItems: 'center', width: 64, height: 64, backgroundColor: '#2e8b57' }}>
        <Text style={{ fontSize: 24, color: '#fff'}}>+</Text>
      </TouchableOpacity>
      </View>
    );
  }

  onChangeTab(tab) {
    this.setState({ tab: tab });
  }

  async camera() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    if (status === 'granted') {
      this.props.navigation.navigate('Camera');
    }

  }
}

const styles = StyleSheet.create({
  tabs: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: Platform.OS === 'android' ? 25 : 0
  },
  tab: {
    width: '50%',
    textAlign: 'center',
    // paddingTop: 10,
    paddingBottom: 10,
    fontSize: 18,
    fontWeight: 'bold',
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    color: '#707070',
  },
  tabActive: {
    borderBottomColor: '#2e8b57',
    borderBottomWidth: 2,
    color: '#2e8b57',
  },
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    // marginTop: 15,
    flex: 1,
    marginBottom: 30,
    backgroundColor: '#f3f4f5'
  },
  item: {
    flexDirection: 'row',
    borderColor: '#2e8b57',
    backgroundColor: '#fff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    padding: 15,
    marginBottom: 10,
  },
  itemInfo: {
    width: '80%',
  },
  itemImage: {
    width: '20%',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: 15
  },
  flexRow: { 
    flexDirection: 'row'
  },
  iconStyle: {
      width: 15, 
      height: 15,
      marginTop: 10,
      marginRight: 10,
      marginLeft: 10
  },
  textStyle: {
      marginTop: 10,
      marginRight: 10,
      marginBottom: 10
  },
  imageHeader: {
    width: 325,
    height: 180,
    marginTop:  10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderRadius: 20
  },
  titleHeader: {
      fontSize: 20,
      color: '#fff',
  },
  subtitleHeader: {
      fontSize: 13,
      marginTop: 5,
      color: '#fff',
  },
});

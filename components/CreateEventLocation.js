import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView } from 'react-native';
import { Constants } from 'expo';
import { Button } from 'react-native-elements';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { Locations } from '../data/locations';
var styles = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputSearch: {
        width: 300,
        height: 46,
        borderColor: '#99abb4',
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        textAlign: 'center',
        marginTop: 10,
    },
    flexRow: {
        flexDirection: 'row'
    },
    iconStyle: {
        width: 20,
        height: 30,
        marginTop: 5,
        marginRight: 10
    },
    textStyle: {
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10
    },
    imageHeader: {
        width: 300,
        height: 180,
        marginTop: 10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    titleHeader: {
        fontSize: 20,
        color: '#fff',
    },
    subtitleHeader: {
        fontSize: 13,
        marginTop: 5,
        color: '#fff',
    }

}

export default class CreateEventLocation extends Component {
    static navigationOptions = {
        title: 'Event Location',
    }
    constructor(props) {
        super(props);
        this.state = {
            locations: Locations,
            value: '',
            search: '',
            text: ''
        };
    }

    componentWillMount() {
        this.setState({ locations: Locations });
        console.log(this.state.locations);
    }

    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <View style={{ flexDirection: 'row', justifyContent: "center", }}>
                        {/* <Image
                            style={ styles.iconStyle }
                            source={require('../assets/icons/placeholder.png')}
                        /> */}
                        <TextInput
                            style={styles.inputSearch}
                            onChangeText={
                                text => onChangeText(this.state.value = text)
                            }
                            placeholder='Please input value'
                            value={this.state.value}
                        />
                    </View>
                    {
                        this.state.locations.map((item, index) => {
                            return (<View style={{ flexDirection: 'row', paddingLeft: 32, paddingRight: 32, paddingTop: 16, paddingBottom: 16 }} key={index}>
                            <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/places.png')}
                            />
                            <View style={{ flexDirection: 'column' }}>
                                <Text>
                                    { item.title }
                                </Text>
                                <Text>
                                    { item.description }
                                </Text>
                            </View>
                        </View>)
                        }
                        )
                    }
                </View>


            </ScrollView>
        );
    }
}
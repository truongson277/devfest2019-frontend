import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView, TouchableOpacity, AsyncStorage, ActivityIndicator } from 'react-native';
import { Constants } from 'expo';
import { Button } from 'react-native-elements';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { FontAwesome as Icon } from '@expo/vector-icons';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import DateTimePicker from "react-native-modal-datetime-picker";
import { send } from '../api/send';
import Moment from 'moment';
var styles = {
    container: {
        // flex: 1, 
        justifyContent: "center",
        alignItems: 'center',
    },
    signIn: {
        backgroundColor: '#2e8b57',
        borderRadius: 30,
        // textAlign: 'center',
        // justifyContent:'center',
        paddingTop: 20,
        paddingBottom: 20,
        width: 300
    },
    signInText: {
        fontSize: 16,
        borderRadius: 30,
        color: '#fff',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    cancel: {
        backgroundColor: '#fff',
        borderRadius: 30,
        textAlign: 'center',
        paddingTop: 20,
        paddingBottom: 20,
    },
    cancelText: {
        fontSize: 16,
        borderRadius: 30,
        color: '#5f6d77',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    iconStyle: {
        width: 24, 
        height: 24,
        marginRight: 10,
        marginLeft: 10
    }
}
export default class CreateEvent extends Component {
    static navigationOptions = {
        title: 'Create Event',
    }
    constructor(props) {
        super(props);
        this.state = {
            time_start: '',
            title: '',
            myAddress: '',
            isDateTimePickerVisible: false,
            photo: {
            }
        };
    }

    async componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener('didFocus', async (payload) => {
            const myLocation = JSON.parse(await AsyncStorage.getItem('myLocation'));
            const myAddress = await AsyncStorage.getItem('myAddress');
            const userInfo = JSON.parse(await AsyncStorage.getItem('userInfo'));
            const photo = this.props.navigation.getParam('photo');
    
            const usersData = this.props.navigation.getParam('users');
            console.log(photo);
            console.log('@@@ user data ' + usersData);
            this.setState({ photo: photo });
            console.log('@@@@ photo' + photo);
            this.setState({ myLocation: myLocation});
            this.setState({ myAddress: myAddress });
            
            this.setState({ userInfo: userInfo });
            this.setState({
                uri: {
                    uri: this.state.userInfo.picture.data.url
                }
            });     
        });
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };
    
    hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
    this.setState({ time_start: Moment(date).format('YYYY-MM-DD hh:mm') })
    this.hideDateTimePicker();
    };

    createEvent() {
        // call API
        this.setState({ isReady: true });
        const body = new FormData();
        // body.append('event[latitude]', this.state.myLocation.lat);
        // body.append('event[longitude]', this.state.myLocation.lng);
        // body.append('event[time_start]', this.state.time_start);
        // body.append('event[title]', this.state.title);
        // body.append('event[address]', this.state.myAddress);
        // body.append('event[image]', {
        //     uri: this.state.photo.uri,
        //     type: 'image/jpg',
        //     name: 'test.jpg'
        // });
        body.append('event[latitude]', 123);
        body.append('event[longitude]', 456);
        body.append('event[time_start]', '15-08-2019');
        body.append('event[title]', 'abc');
        body.append('event[address]', 'trongpq');
        body.append('event[image]', {
            uri:  this.state.photo.uri,
            type: 'image/jpg',
            name: 'test.jpg'
        });
        console.log(body);
        send('POST', {
            url: '/api/v1/events', body: body
        }).then(res => {
            console.log(res);
            if (res.status === 200) {
                this.setState({ isReady: false });
                Alert.alert(
                    'EVENT',
                    'Thanks for your event!',
                    [
                        {text: 'OK', onPress: () => {  this.props.navigation.navigate('EventList2'); }},
                    ]
                    );
               
            }
        }).catch(err => {
            this.setState({ isReady: false });
            alert(err);
        })
    }

    async camera() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        if (status === 'granted') {
          this.props.navigation.navigate('cameraEvent');
        }
    }

    render() {
        Moment.locale('en');
        return (
            <ScrollView>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'column', paddingLeft: 32, paddingRight: 32, paddingTop: 16, paddingBottom: 16 }}>
                        { this.state.photo ? (
                                <Image resizeMode={'contain'}
                                    style={{
                                        width: '100%',
                                        height: 220

                                    }}
                                    source={{ uri: this.state.photo.uri }}
                                />
                                ): null
                        }
                        <View style={{ flexDirection: 'row', justifyContent:'space-between', marginTop: 10, marginBottom: 10 }}>
                        <Text>
                         Title:  
                            </Text><TextInput style={{ borderColor: '#ddd', width: 150, marginLeft: 15, borderWidth: 1, paddingLeft: 10, paddingRight: 10 }}
                            >
                            </TextInput>
                            <TouchableOpacity onPress={() => {
                                this.camera();
                            }} style={ styles.iconStyle }>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/camera.png')}
                                />
                            </TouchableOpacity>
                        </View>
                        
                        <TouchableOpacity onPress={this.showDateTimePicker} style={{ flexDirection: 'row', justifyContent: 'flex-start', alignContent: 'flex-start', marginTop: 10, marginBottom: 10 }}>
                            <Text>
                            Time:  
                            </Text>
                            <Text style={{
                                 marginLeft: 5,
                                 width: 200,
                                 borderBottomColor: '#ddd',
                                 borderBottomWidth: 1,
                                 borderStyle: 'solid',
                                 textAlign: 'center'
                                 }}>{this.state.time_start}</Text>
                            <DateTimePicker
                            is24Hour={true}
                            mode={'datetime'}
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                            />
                        </TouchableOpacity>
                        <Text>Position: {this.state.myAddress}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between', marginTop: 10, marginBottom: 10 }}>
                        
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignContent: 'space-between', marginTop: 10, marginBottom: 10 }}>
                            <Text>
                            Description
                            </Text>
                        </View>
                        <TextInput
                            style={{ paddingBottom: 80, paddingLeft: 10, marginTop: 10, marginBottom: 10, alignItems: 'flex-start', justifyContent: 'flex-start', borderColor: '#99abb4', borderWidth: 1, borderRadius: 5 }}
                            placeholder="Let's we know how your feeling ..."
                            multiline
                            value={this.state.note}
                            onChangeText={(note) => this.setState({ note })}
                        />
                        <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignContent: 'space-between' }}>
                            <Text
                            onPress={() =>
                                this.props.navigation.navigate('CreateEventInvite')
                            }
                            >
                            Invite
                            </Text>
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <View style={styles.signIn}>
                            <Text style={styles.signInText} onPress={ () => { this.createEvent() } }>Submit</Text>
                        </View>
                        <View style={styles.cancel}>
                            <Text style={styles.cancelText} onPress={ () => { console.log('@@@@@'); } }>Cancel</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
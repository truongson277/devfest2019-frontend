/**
 * docs
 * - Login with facebook: https://docs.expo.io/versions/latest/sdk/facebook/
 * - Login with google: https://docs.expo.io/versions/latest/sdk/google/
 * - generate key hash [keytool -list -printcert -jarfile YOUR_APK.apk | grep SHA1 | awk '{ print $2 }' | xxd -r -p | openssl base64]
 *
 */
import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Platform,
  TouchableOpacity
} from 'react-native';
import { send } from '../api/send';
// import { Google } from 'expo';
export default class History extends React.Component {
  constructor(props) {
    super(props);
}
  items = [1,2,3,4,5,6,7,8,9,10];
  state = {
    email: '',
    password: '',
    histories: {
      today: [],
      oldday: []
    }
  };

  static navigationOptions = {
    title: 'History'
  }

  componentWillMount() {
    this.getHistory();
  }

  getHistory() {
    send('GET', { url: '/api/v1/reports/histories' }).then(res => {
      if (res.status === 200) {
        const histories = {};
        histories['today'] = res.data.filter(history => history.attributes.group_date === 'today');
        histories['oldday'] = res.data.filter(history => history.attributes.group_date === 'oldday');
        this.setState({ histories: histories });
      }
    })
  }

  render() {
    return (
      <ScrollView style={styles.page}>
        <View style={styles.container}> 
            <Text style={styles.title}>Today</Text> 
            {this.state.histories.today.map((history,index)=> {
            return (
              <TouchableOpacity style={styles.item} key={index} onPress={()=> this.props.navigation.navigate('HistoryDetail', { report_id: history.id })}>
                <Text style={styles.address}>{history.attributes.address}</Text>
                <Text style={[styles.status, styles.checked]}>{history.attributes.report_type}</Text>
              </TouchableOpacity>
            )
            })}

            <Text style={styles.title}>Oldday</Text> 
            {this.state.histories.oldday.map((history,index)=> {
            return (
              <TouchableOpacity style={styles.item} key={index}>
                <Text style={styles.address}>{history.attributes.address}</Text>
                <Text style={[styles.status, styles.checked]}>{history.attributes.report_type}</Text>
              </TouchableOpacity>
            )
            })}
        </View>
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#f3f4f5',
    paddingTop: Platform.OS === 'android' ? 25 : 0,
  },
 container: {
  paddingBottom: 60,
  paddingLeft: 15,
  paddingRight: 15,
 },
 headerTitle: {
   fontSize: 18,
   fontWeight: 'bold',
  paddingTop: 20,
  paddingBottom: 20,
  justifyContent: 'center',
  textAlign: 'center',
  alignItems: 'center',
  backgroundColor: '#fff'
},
 title: {
   marginTop: 20,
   marginBottom: 10,
   fontSize: 15,
   color: '#99abb4'
 },
 checked: {
  color: '#61bc84'
 },
 item: {
  backgroundColor: '#fff',
  padding: 15,
  marginBottom: 10
 },
 address: {
   fontSize: 17,
   color: '#5f6d77',
   marginBottom: 10
 },
 status: {
   fontSize: 13,
   textTransform: "capitalize"
 }
});

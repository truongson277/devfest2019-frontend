import React, { Component } from 'react';
import { Alert, Text, View, TextInput, Image, ScrollView, SafeAreaView, TouchableOpacity, Platform, } from 'react-native';
import { Constants } from 'expo';
import { Button } from 'react-native-elements';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import '@expo/vector-icons';
import { FontAwesome as Icon } from '@expo/vector-icons';

var styles = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 30
    },
    button: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#1E6738',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        marginLeft: 30,
        marginBottom: 20
    },
    cancel: {
        width: 300,
        height: 44,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        marginLeft: 30
    },
    inputSearch: {
        width: 300,
        height: 46,
        borderColor: '#fff',
        backgroundColor: '#fff',
        borderRadius: 23,
        borderWidth: 1,
        textAlign: 'center',
        marginTop: 10,
    },
    flexRow: { 
        flexDirection: 'row'
    },
    iconStyle: {
        width: 15, 
        height: 15,
        marginTop: 10,
        marginRight: 10,
        marginLeft: 10
    },
    textStyle: {
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10
    },
    imageHeader: {
        width: 300,
        height: 180,
        marginTop:  10,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderRadius: 20
    },
    titleHeader: {
        fontSize: 20,
        color: '#fff',
    },
    subtitleHeader: {
        fontSize: 13,
        marginTop: 5,
        color: '#fff',
    },
    tabs: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: Platform.OS === 'android' ? 25 : 0
      },
      tab: {
        width: '50%',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 18,
        fontWeight: 'bold',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        color: '#707070',
      },
      tabActive: {
        borderBottomColor: '#2e8b57',
        borderBottomWidth: 2,
        color: '#2e8b57',
      },

}

export default class EventList extends Component {
    static navigationOptions = {
        title: 'Events',
    }
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            search: '',
            text: ''
        };
    }
    
    render() {
        const { search, text } = this.state;
        return (
            <View>
            <ScrollView>
            <SafeAreaView style={[styles.tabs]}>
            <Text

                onPress={() => {
                this.onChangeTab(1);
                }}
                style={[styles.tab, this.state.tab === 1 ? styles.tabActive : null]}>
                This Week
            </Text>
            <Text
                onPress={() => {
                this.onChangeTab(2);
                }}
                style={[styles.tab, this.state.tab === 2 ? styles.tabActive : null]}>
                Up Coming{' '}
            </Text>
            </SafeAreaView>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#f3f4f5', paddingTop: 20, position: 'relative' }}>
                    <View style= {{ flexDirection: 'row'}}>
                        <TextInput
                        style={ styles.inputSearch }
                        onChangeText={
                            text => onChangeText( this.state.value = text)
                        }
                        placeholder='Please input value'
                        value={this.state.value}
                        />
                        <Icon
                        style= {{ marginLeft: -35, marginTop: 18 }}
                        size={28}
                        name='search' />
                    </View>
                    <View style={{ marginTop: 5, marginBottom: 5}}>
                        <Image
                        style={ styles.imageHeader }
                        source={{uri: 'https://images.unsplash.com/photo-1570654282300-9e8952986614?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80'}}
                        />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: -40 }}>
                        <Text style={ styles.titleHeader }>
                        Fireworks Night festival
                        </Text>
                        <Text style={ styles.subtitleHeader }>
                        2 miles
                        </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' }}>
                            <View style={ styles.flexRow}>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/calendar.png')}
                                />
                                <Text style={ styles.textStyle }>
                                Mon, 10 Oct 2019
                                </Text>
                            </View>
                            <View style={ styles.flexRow }>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/time.png')}
                                />
                                <Text style={ styles.textStyle }>
                                19:00
                                </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderBottomColor: 'rgba(0,0,0,0.16)', borderBottomWidth: 0.5, paddingBottom: 10 }}>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/placeholder.png')}
                                />
                                <Text style={ styles.textStyle }>
                                Bach Dang, Nai Hien Dong, Da Nang
                                </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                            <View style={ styles.flexRow}>
                                <Image
                                style={{ width: 30, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                                source={require('../assets/icons/threePeople.png')}
                                />
                                <Text style={ styles.textStyle }>
                                42 Interested
                                </Text>
                            </View>
                            <View style={ styles.flexRow }>
                                <Image
                                style={{ width: 20, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                                source={require('../assets/icons/eye.png')}
                                />
                                <Text style={ styles.textStyle }>
                                210 views
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ marginTop: 5, marginBottom: 5}}>
                        <Image
                        style={ styles.imageHeader }
                        source={{uri: 'https://images.unsplash.com/photo-1570654282300-9e8952986614?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1868&q=80'}}
                        />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: -40 }}>
                        <Text style={ styles.titleHeader }>
                        Fireworks Night festival
                        </Text>
                        <Text style={ styles.subtitleHeader }>
                        2 miles
                        </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' }}>
                            <View style={ styles.flexRow}>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/calendar.png')}
                                />
                                <Text style={ styles.textStyle }>
                                Mon, 10 Oct 2019
                                </Text>
                            </View>
                            <View style={ styles.flexRow }>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/time.png')}
                                />
                                <Text style={ styles.textStyle }>
                                19:00
                                </Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', backgroundColor: '#fff', borderBottomColor: 'rgba(0,0,0,0.16)', borderBottomWidth: 0.5, paddingBottom: 10 }}>
                                <Image
                                style={ styles.iconStyle }
                                source={require('../assets/icons/placeholder.png')}
                                />
                                <Text style={ styles.textStyle }>
                                Bach Dang, Nai Hien Dong, Da Nang
                                </Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                            <View style={ styles.flexRow}>
                                <Image
                                style={{ width: 30, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                                source={require('../assets/icons/threePeople.png')}
                                />
                                <Text style={ styles.textStyle }>
                                42 Interested
                                </Text>
                            </View>
                            <View style={ styles.flexRow }>
                                <Image
                                style={{ width: 20, height: 15, marginTop: 10, marginRight: 10, marginLeft: 10 }}
                                source={require('../assets/icons/eye.png')}
                                />
                                <Text style={ styles.textStyle }>
                                210 views
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
            <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('CreateEvent');
                }} style={{ position: 'absolute', bottom: '5%', left: '40%', borderRadius: 32, zIndex: 9999999999999, 
                backgroundColor: '#2e8b57',
                justifyContent: 'center', 
                alignItems: 'center', 
                width: 64, 
                height: 64,

                 }}>
                <Text style={{ 
                    width: 30, 
                    height: 30, 
                    textAlign: 'center',
                    fontSize: 25,
                    color: '#fff'
                }}>+</Text>
            </TouchableOpacity>
            </View>
        );
    }
}
/**
 * docs
 * - Login with facebook: https://docs.expo.io/versions/latest/sdk/facebook/
 * - Login with google: https://docs.expo.io/versions/latest/sdk/google/
 * - generate key hash [keytool -list -printcert -jarfile YOUR_APK.apk | grep SHA1 | awk '{ print $2 }' | xxd -r -p | openssl base64]
 *
 */
import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  Button,
  ScrollView,
  Platform,
  AsyncStorage
} from 'react-native';
import Icon from '@expo/vector-icons/FontAwesome';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
// import { Google } from 'expo';
export default class Login extends React.Component {
  constructor(props) {
    super(props);
}
  state = {
    email: '',
    password: '',
  };

  async onLogin(type) {
    try {
      if (type === 'fb') {
        Facebook.logInWithReadPermissionsAsync('1087566321631603', {
          permissions: ['public_profile'],
        }).then(res => {
          const token = res.token;
          // graph facebook API
          fetch(`https://graph.facebook.com/me?fields=id,name,email,picture&access_token=${token}`).then(data => {
            data.json().then(async dt => {
              const user = dt;
              await AsyncStorage.setItem('userInfo', JSON.stringify(user));
              this.props.navigation.navigate('Map');
            });
          });

        }).catch(err => {
          alert(err);
        })
      }

    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  login() {
    this.props.navigation.navigate('map');
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.logo}>
          <Image
            style={{ width: 200, height: 200 }}
            source={require('../assets/logo-login.png')}
          />
        </View>
        <View style={styles.inputContainer}>
          <Icon style={styles.icon} name="envelope" size={24} color="#2e8b57" />
          <TextInput style={styles.inputStyle} placeholder="Email" />
        </View>
        <View style={styles.inputContainer}>
          <Icon
            style={styles.icon}
            name="unlock-alt"
            size={35}
            color="#2e8b57"
          />
          <TextInput
            underlineColorAndroid="transparent"
            style={styles.inputStyle}
            placeholder="Password"
          />
        </View>
        <Text style={styles.forgot}>For got your password?</Text>
        <View style={styles.signIn}>
          <Text style={styles.signInText} onPress={ () => this.login() }>Sign in</Text>
        </View>
        <Text style={{ textAlign: 'center', fontSize: 13, marginTop: 30 }}>
          Or connect using social account{' '}
        </Text>
        <View style={styles.socialIcon}>
          <Text style={{ width: 30 }} onPress={ () => this.onLogin('gg') }>
            <Icon name="google" size={20} color="#2e8b57" />
          </Text>
          <Text style={{ width: 30 }} onPress={ () => this.onLogin('fb') }>
            <Icon name="facebook" size={20} color="#2e8b57" />
          </Text>
        </View>
        <Text style={{ textAlign: 'center', fontSize: 13, marginTop: 30 }}>
          Dont' have a WAG account?{' '}
          <Text style={{ color: '#2e8b57' }}>Join with us now</Text>
        </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
  },
  container: {
    paddingTop: Platform.OS === 'android' ? 25 : 0,
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  inputContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    borderColor: '#ccc',
    borderStyle: 'solid',
    borderWidth: 1,
    paddingTop: 15,
    paddingBottom: 15,
    borderRadius: 5,
    marginBottom: 40,
  },
  icon: {
    width: 80,
    justifyContent: 'center',
    textAlign: 'center',
  },
  inputStyle: {
    width: '100%',
    fontSize: 14,
    color: '#767676',
  },
  forgot: {
    textAlign: 'right',
    color: '#707070',
    fontSize: 13,
  },
  signIn: {
    backgroundColor: '#2e8b57',
    borderRadius: 30,
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 40,
  },
  signInText: {
    fontSize: 16,
    borderRadius: 30,
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  socialIcon: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

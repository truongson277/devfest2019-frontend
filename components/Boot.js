import React, { Component } from 'react'
import {
    Text,
    View,
    Image
} from 'react-native';
import Swiper from 'react-native-swiper';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

var styles = {
    wrapper: {
    },
    bigTitle: {
        fontWeight: 'bold',
        fontSize: 32,
    },
    subTitle: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center'
    },
    skip: {
        marginTop: 30,
        fontSize: 18,
        color: '#b1b1b1',
    },
}

export default class BootSplash extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        };
    }
    componentWillMount() {
        // Permissions.askAsync(Permissions.NOTIFICATIONS).then(status => {
        //     if (status.status === 'granted') {
        //         Notifications.getDevicePushTokenAsync()
        //             .then(token => {
        //                 alert(token);
        //             })
        //             .catch(err => {
        //                 console.log(err);
        //             });

        //     }
        // });
    }
    render() {
        return (
            <Swiper style={styles.wrapper} showsButtons={false} loop={false}>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: 250, height: 250 }}
                        source={require('../assets/step1.png')}
                    />
                    <Text style={styles.bigTitle}>Selfie With Garbage</Text>
                    <Text style={styles.subTitle}>Lorem ipsum dolor sit amet,
                      consetetur sadipscing elitr
                    </Text>
                    <Text style={styles.skip}
                        onPress={() =>
                            this.props.navigation.navigate('Permission')
                        }
                    >Skip</Text>
                </View>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: 250, height: 250 }}
                        source={require('../assets/step2.png')}
                    />
                    <Text style={styles.bigTitle}>Selfie With Garbage</Text>
                    <Text style={styles.subTitle}>Lorem ipsum dolor sit amet,
                      consetetur sadipscing elitr
                    </Text>
                    <Text style={styles.skip}
                        onPress={() =>
                            this.props.navigation.navigate('Permission')
                        }
                    >Skip</Text>
                </View>
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: 250, height: 250 }}
                        source={require('../assets/step3.png')}
                    />
                    <Text style={styles.bigTitle}>Selfie With Garbage</Text>
                    <Text style={styles.subTitle}>Lorem ipsum dolor sit amet,
                      consetetur sadipscing elitr
                    </Text>
                    <Text style={styles.skip}
                        onPress={() =>
                            this.props.navigation.navigate('Permission')
                        }
                    >Skip</Text>
                </View>
            </Swiper>
        );
    }
}
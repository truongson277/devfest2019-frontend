import React from 'react';
import { Text, View, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import { Camera } from 'expo-camera';
export default class CameraApp extends React.Component {
  static navigationOptions = {
      title: 'Reports',
  }
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    isReady: false,
  };

  async componentDidMount() {
    // console.log('load');
    // const { status } = await Permissions.askAsync(Permissions.CAMERA);
    // this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => {
          this.camera = ref;
        }}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              style={{
                position: 'absolute',
                left: '20%',
                top: 30,
                zIndex: 999999
              }}
              onPress={() => {
                this.setState({
                  type:
                    this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                });
              }}>
              {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>CAMERA TRUOC</Text> */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                position: 'absolute',
                left: '40%',
                bottom: 30,
                zIndex: 999999
              }}
              onPress={() => {
                if (this.camera) {
                  this.setState({ isReady: true });
                  this.camera.takePictureAsync().then(photo => {
                    this.setState({ isReady: false });
                    this.props.navigation.navigate('CreateEvent', {
                      photo: photo
                    });
                  })
                }
              }
              }
            >
              <Text style={{
                color: '#fff',
                backgroundColor: '#fff',
                width: 80,
                height: 80,
                borderRadius: 40
              }}></Text>
            </TouchableOpacity>
          </View>
        </Camera>
        {this.state.isReady ? (<View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>) : null}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: '40%',
    top: '40%',
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})
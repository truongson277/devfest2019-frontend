import Map from './components/Map';
import ScannerQRcode from './components/ScannerQRcode';
import Login from './components/Login';
import { createAppContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Boot from './components/Boot';
import Permission from './components/Permisstion';
import Report from './components/Report';
import CameraApp from './components/Camera';
import CameraEvent from './components/CameraEvent';
import CreateEvent from './components/CreateEvent';
import CreateEventLocation from './components/CreateEventLocation';
import CreateEventInvite from './components/CreateEventInvite';
import EventList2 from './components/EventList2';
import * as React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { rootReducer } from './redux/reducer';
import ReportComplete from './components/ReportComplete';
import TabBarBottom from './components/TabBarBottom';
import History from './components/History';
import HistoryDetail from './components/HistoryDetail';
const store = createStore(rootReducer);
import { Image } from "react-native";
const bottomTabContainer = createStackNavigator(
  {
    boot: {
      screen: Boot,
      navigationOptions: {
        header: null,
      }
    },
    login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Permission: {
      screen: Permission,
      navigationOptions: {
        tabBarVisible: false
      }
    },
    Report: {
      screen: Report
    },
    ReportComplete: {
      screen: ReportComplete
    },
    Map: {
      screen: Map,
      navigationOptions: {
        header: null,
      }
    },
    // EventList: {
    //   screen: EventList
    // },
    Camera: {
      screen: CameraApp,
      navigationOptions: {
        header: null,
      }
    },
    History: {
      screen: History
    },
    HistoryDetail: {
      screen: HistoryDetail
    },
    Report: {
      screen: Report
    },
    ScannerQRcode: {
      screen: ScannerQRcode,
      navigationOptions: {
        header: null,
      }
    },
    CreateEvent: {
      screen: CreateEvent
    },
    CreateEventInvite: {
      screen: CreateEventInvite
    },
    createEventLocation: {
      screen: CreateEventLocation
    },
    EventList2: {
      screen: EventList2
    },
    cameraEvent: {
      screen: CameraEvent,
      navigationOptions: {
        header: null,
      }
    }
  },
  {
    initialRouteName: 'CreateEvent',
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
      activeTintColor: "#4F4F4F",
      inactiveTintColor: "#ddd"
    }
  },
)


const MainApp = createBottomTabNavigator(
  {
    Map: bottomTabContainer,
    History: History,
    Events: EventList2,
    Report: CameraApp
    // Settings: SettingsTab ,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === 'Map') {
          return (
            <Image
              source={ require('./assets/tabbar/map.png') }
              style={{ width: 20, height: 20, }} />
          );
        } else if (routeName === 'History'){
          return (
            <Image
            source={ require('./assets/tabbar/list.png') }
            style={{ width: 20, height: 20, }} />
          );
        } else if (routeName === 'Events') {
          return (
          <Image
          source={ require('./assets/tabbar/event.png') }
          style={{ width: 20, height: 20, }} />
          )
        } else if (routeName === 'Report') {
          return (
          <Image
          source={ require('./assets/tabbar/report.png') }
          style={{ width: 20, height: 20, }} />
          )
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#2e8b57',
      inactiveTintColor: '#263238',
    },
  }
);

const AppContainer = createAppContainer(MainApp)



export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}

export const Makers = [
    { id: 1, lat: 16.047079, lng: 108.219955, status: 'ok', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 2, lat: 16.047079, lng: 108.209955, status: 'ok', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 3, lat: 16.037079, lng: 108.199955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 4, lat: 16.027079, lng: 108.199955, status: 'danger', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 5, lat: 16.045079, lng: 108.198955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 6, lat: 16.045607, lng: 108.197955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 7, lat: 16.034507, lng: 108.196955, status: 'danger', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 8, lat: 16.047079, lng: 108.195955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 9, lat: 16.047079, lng: 108.194955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' },
    { id: 10, lat: 16.047079, lng: 108.193955, status: 'warning', addr: 'Số 15 Hai Bà Trưng - Da Nang' }
];
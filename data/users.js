export const Users = [
    { id: 1, username: 'Phan Quoc Trong', invite: false, avatar: {uri: 'https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 2, username: 'Hoang Tuan Dat', invite: false, avatar:  {uri: 'https://images.unsplash.com/photo-1524593689594-aae2f26b75ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'}},
    { id: 3, username: 'Vo Truong Son', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 4, username: 'Nguyen Tam Phu', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 5, username: 'Hoang Trong Nghia', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1544725176-7c40e5a71c5e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 6, username: 'Hoang Trong Khoi', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1569913486515-b74bf7751574?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 7, username: 'Vo Van Duc', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1552673304-7fe7389c2932?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 8, username: 'Nguyen Thanh Trung', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1552673304-23f6ad21aada?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 9, username: 'Doan Van Duc', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1528763380143-65b3ac89a3ff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }},
    { id: 10, username: 'Phu Le', invite: false, avatar:  {uri:'https://images.unsplash.com/photo-1544724107-6d5c4caaff30?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60' }}
];
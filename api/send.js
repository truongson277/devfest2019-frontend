
export const send = (method, options) => {
    return new Promise((resolve, reject) => {
        const _options = {
            method: method,
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        }
        console.log(options);
        if (options.body) {
            _options.body = options.body;
        }
        fetch('http://134.209.110.79' + options.url, _options).then(res => {
            resolve(res.json());
        }).catch(err => {
            reject(err);
        })
    });
}
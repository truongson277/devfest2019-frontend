var firebase = require("firebase-admin");

var serviceAccount = require("./service-accounts.json");

var payload = {
    notification: {
        title: "Account Deposit",
        body: "A deposit to your savings account has just cleared."
    },
    data: {
        account: "Savings",
        balance: "$3020.25"
    }
};

const device_token = 'ExponentPushToken[QwySJrOXBoiJYzt6ZuCWnT]';
const options = {
    priority: "high",
    timeToLive: 60 * 60 *24
  };
firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://fir-notification-b3433.firebaseio.com"
});

firebase.messaging().sendToDevice(device_token, payload, options).then(res => {
    console.log('successfully sent message: ', res.results);
}).catch(err => {
    console.log(err);
});
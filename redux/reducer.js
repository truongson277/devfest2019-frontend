import  { initialState, URL_IMAGE_CATURE  } from './state';
export function rootReducer(state= initialState, action) {
    switch(action.type) {
        case URL_IMAGE_CATURE: {
            Object.assign({}, state, { url_image_cature: action.payload });
        }
    }
}